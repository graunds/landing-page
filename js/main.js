import "whatwg-fetch";
import "@babel/polyfill";

const subscribeForm = document.getElementById("subscribe-form");
const joinTitle = document.getElementById("join-title");
const formWrapper = document.querySelector(".form-wrapper");
const formError = document.querySelector(".error");

import "../css/main.scss";

if (~document.cookie.indexOf("subscribed")) {
  subscribeForm.parentNode.removeChild(subscribeForm);
  joinTitle.parentNode.removeChild(joinTitle);

  formWrapper.innerHTML = `
    <p class="subscribed">
      <i class="icon">🎉</i>
      <br />
      Thank you for your interest!
      <br />
      We'll let you know when Graunds is launching.
    </p>
  `;
}

subscribeForm.addEventListener("submit", e => {
  e.preventDefault();

  fetch(
    "https://docs.google.com/forms/d/e/1FAIpQLScKTB_XfFVtGW6xyXfggt6tjet4tWoaB7zTa5uMpayoEnRlpg/formResponse",
    {
      method: "post",
      headers: { "Content-Type": "application/json" },
      body: new FormData(subscribeForm),
      mode: "no-cors"
    }
  )
    .then(response => {
      document.cookie = "subscribed=true;max-age=31536000;samesite=strict";
      subscribeForm.parentNode.removeChild(subscribeForm);

      formWrapper.innerHTML = `
          <span class="subscribed">
            <span class="icon">🎉</span>
            <br />
            Thank you for your interest!
            <br />
            We'll let you know when Graunds is ready to open.
          </span>
        `;
    })
    .catch(err => {
      formError.innerHTML =
        "We weren't able to add your email. Please try again and if the error persists, you can email us at hello@graunds.com and we'll add you manually. Sorry for the inconvenience 🙏";
    });
});
